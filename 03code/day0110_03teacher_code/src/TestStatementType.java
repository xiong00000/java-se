/*
语句分为两大类：
单语句：
    独立一条语句，例如：System.out.println("hello");
    特殊的单语句，空语句，就是;
复合语句：
     多句语句组成一个整体。通常都会用{}把多个语句包围起来。
     例如：if...else，switch...case，循环等

复合语句因为有很多语句组成，所以有流程控制语句概念：
（1）顺序结构：从上往下依次执行
（2）分支结构：多个语句可能只执行其中的一个
（3）循环结构：某个语句会被“反复”/"重复"执行
（4）跳转/中断：break,continue,return等


 */
public class TestStatementType {
    public static void main(String[] args) {

    }
}
