//4、运行时从键盘输入两个数，程序可以将它们按照从小到大的顺序输出
class Exam4{
	public static void main(String[] args){
		java.util.Scanner input = new java.util.Scanner(System.in);
		System.out.print("请输入第一个整数：");
		int a = input.nextInt();
		System.out.print("请输入第二个整数：");
		int b = input.nextInt();
		//前面五句代码表示运行时从键盘输入了两个整数分别存放到了a,b变量中
		//补充代码
		
		/*
		int max = a > b ? a : b;
		int min = a < b ? a : b;
		System.out.println(min + "," + max);*/
		
		/*
		if(a>b){
			System.out.println(b + "," + a);
		}else{
			System.out.println(a + "," + b);
		}*/
		
		/*
		if(a>b){
			int temp = a;
			a = b;
			b = temp;
		}
		System.out.println(a + "," + b);*/
	}
}