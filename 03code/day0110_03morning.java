//1、请计算9与7按位与、按位或、按位异或的结果


//2、请判断以下程序的输出结果
class Exam2{
	public static void main(String[] args){
		int i = 1;
		int j = 2;
		i += j++;
		j = i++ / 2 + ++i * j--;
		System.out.println("i = " + i);
		System.out.println("j = " + j);
	}
}
//3、运行时从键盘输入一个整数，
//程序可以判断一个整数是否是3的倍数或者以3结尾，如果是就打印true，否则打印false
class Exam3{
	public static void main(String[] args){
		java.util.Scanner input = new java.util.Scanner(System.in);
		System.out.print("请输入一个整数：");
		int num = input.nextInt();
		//前面三句代码表示运行时从键盘输入了一个整数存放到了num变量中
		//补充代码
	}
}
//4、运行时从键盘输入两个数，程序可以将它们按照从小到大的顺序输出
class Exam4{
	public static void main(String[] args){
		java.util.Scanner input = new java.util.Scanner(System.in);
		System.out.print("请输入第一个整数：");
		int a = input.nextInt();
		System.out.print("请输入第二个整数：");
		int b = input.nextInt();
		//前面五句代码表示运行时从键盘输入了两个整数分别存放到了a,b变量中
		//补充代码
	}
}
//5、补充代码实现将一个小写字母转换为大写字母输出
class Exam5{
	public static void main(String[] args){
		char letter = 'g';
		//补充代码
	}
}